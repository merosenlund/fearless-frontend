window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do if the response is bad
        } else {
            const data = await response.json();
            const conferences = data.conferences[0];
            const nameTag = document.querySelector('.card-title');
            nameTag.innerHTML = conferences.name;

            const detailUrl = `http://localhost:8000${conferences.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const conference = details["conference"];
                const descriptionTag = document.querySelector(".card-text");
                descriptionTag.innerHTML = conference.description;
                const imageTag = document.querySelector(".card-img-top");
                imageTag.src = conference.location.picture_url;
            }
        }
    } catch (e) {
        // Figure out what to do if the an error is raised
        console.log(e)
    }
});
